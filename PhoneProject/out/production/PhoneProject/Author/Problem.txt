Clasa Author.
-name: String
-email: String
-gender: char

**Trei instante de variabile PRIVATE (apropo gender - 'f' sau 'm')
-----------------
+Author(name: String, email: String, gender: char)
+getName(): String
+getEmail(): String
+setEmail(email: String): void
+getGender(): char
+toString(): String
----------------------------
**Avem nevoie de un consturctor ca sa initializam name, email si gender cu valorile date

public Author(String name, String email, char gender){....}

**public getters/setters: getName(), getEmail(), setEmail() si getGender()

**metoda toString() care returneaza "numele-autorului (gender-ul), email" - exemplu "Alank Kay (m) alankay@books.com"

Trebuie sa scrieti/creati clasa Author. De asemenea creati un program pentru test numit TestAuthor in care sa testati
constructorul si metodele publice.
Incercati sa schimbati email-ul unui autor;
----------------------------------------
Author anAuthor = new Author("Alan Kay", "alankay@books.com", 'm');
System.out.println(anAuthor); //call toString()
anAuthor.setEmail("alankay88@gmail.com");
System.out.println(anAuthor);
----------------------------

----------------------------
O clasa numita Book. Ea contine:

** 4 instante private de variabila: name(String), author (creat adineaori - acel anAuthor),
price(double), qtyInStock();

**Doi constructori:

public Book(String name, Author author, double price) {....}

public Book(String name, Author author, double price, int qtyInStock){......}

------------------------------
metodele publice:
getName()
getAuthor()
getPrice()
setPrice()
getQtyInStock()
setQtyInStock()

-------------------------------
Creati clasa Book (care utilizeaza clasaAuthor creata mai devreme)De asemenea creati un program pentru test
numit TestBook, pentru a testa constructorul, cat si metodele publice ale clasei Book. Mentionam ca trebuie sa
fie creata o instanta de autor inainte de o instanta a clasei Book
-------------------------------------------
Author anAuthor = new Author(...........);
Book aBook = new ("Java pentru incepatori", anAuthor, 19.95, 1000)
//Utilizeaza o instanta anonima a lui Author
Book anotherBook = new Book ("mai multe Java pentru incepatori", new Author(......), 29.95, 850);
-------------------------------
Aveti in vedere ca ambele (Book si Author) au cate o variabila numita name. Totusi se poate face diferentierea prin
instanta referentiala. Pentru Book instanta spune aBook.name si se refera la un nume de carte;
pentru autor instanta spune anAuthor - si anume anAuthor.name si se refera la numele autorului.

INCERCATI SA:

1. Printati numele si e mailul autorului din instanta Book. (Indiciu: aBook.getAuthor().getName(),
aBook.getAuthor().getEmail()).

2. Introduceti noi metode numite getAuthorName(), getAuthorEmail(), getAuthorGender() in clasa Book,
care sa returneze name, email si gender-ul autorului cartii. De exmeplu:

public String getAuthorName() {..........}