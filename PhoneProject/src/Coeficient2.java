import java.util.Scanner;

public class Coeficient2 {

    public static int coeficient(int number1, int number2){
        if (number2 == 0){
            System.out.println("You can not devide by 0.");
            System.exit(1);
        }
        return number1/number2;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter two integers : ");
        int number1 = input.nextInt();
        int number2 = input.nextInt();

        System.out.println(number1 + " / " + number2 + " is " + (number1/number2));
    }
}
