public class Dog {

    int age;
    String name;

    public Dog(){
    }

    public Dog(int age, String name){
        this.age = age;
        this.name = name;
    }

    public String getDogName(){
        return name;
    }
}
