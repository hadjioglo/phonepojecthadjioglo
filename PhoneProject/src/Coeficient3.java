import java.util.Scanner;

public class Coeficient3 {

    public static int coeficient(int number1, int number2){
        if (number2 == 0){
            throw new ArithmeticException("divider could not be 0 ");
        }
        return number1/number2;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter two integers : ");
        int number1 = input.nextInt();
        int number2 = input.nextInt();

        try {
            int result = coeficient(number1, number2);
            System.out.println(number1 + " / " + number2 + " is " + (number1/number2));
        }
        catch (ArithmeticException ex){
            System.out.println("Exception: an integer " + "could not be devided by zero ");
        }
        System.out.println("Execution continues");


    }
}
