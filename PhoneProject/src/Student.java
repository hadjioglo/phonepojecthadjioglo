import java.util.Calendar;
import java.util.Scanner;

public class Student {
    public static void main(String[] args) {
        System.out.println("Intrare in program...");

        Scanner sc = new Scanner(System.in);
        System.out.println();

        System.out.print("Introduce facultatea(Matematica/Informatica): ");
        String facultate = sc.next();
        checkFaculate(facultate);

        System.out.print("Doriti sa introduceti studenti noi? (Da/Nu)");
        String choice = sc.next();
        checkStudent(choice);


        System.out.print("Introduceti Specialitatea (Algebra/Geometrie/Calcul_Integral): ");
        String specialitatea2 = sc.next() ;
        if(specialitatea2.equals("Algebra")||specialitatea2.equals("Geometrie")||specialitatea2.equals("Calcul_Integral")) {
            System.out.print("Introduceti nota test1: ");
            int notaTest1 = sc.nextInt();
            System.out.print("Introduceti nota test2: ");
            int notaTest2 = sc.nextInt();
            System.out.print("Introduceti nota examen: ");
            int notaExamen = sc.nextInt();

            //hadjioglo code
            calculateMedie(notaTest1, notaTest2, notaExamen);
            //hadjioglo code ends here

        }else {
            System.exit(0);
        }
        System.out.println("Iesire din program...");
    }

    private static void calculateMedie(int notaTest1, int notaTest2, int notaExamen){
        double medie = 0;
        medie = (notaTest1 + notaTest2 + notaExamen ) /3.0;

        if (medie > 9){
            System.out.println();
            System.out.println("Esti Promovat!");
        }else System.out.println("Nu ati trecut cu brio, incercati in semestrul urmator");
        System.out.println();
    }

    private static void checkFaculate(String facultate){
        String facultateChanged = facultate.toLowerCase();

        if (facultateChanged.equals("matematica")) {
            System.out.println("A fost aleasa Facultatea " + facultate);
        } else if (facultateChanged.equals("informatica")) {
            System.out.println("A fost aleasa Facultatea " + facultate);
        } else {
            System.out.println("You have entered wrong facultate");
            System.out.println("Iesire din program...");
            System.exit(0);

        }
    }

    private static void checkStudent(String choice) {
        Scanner sc = new Scanner(System.in);
        String grupa = null;
        if(choice.equals("Da")) {
            System.out.print("Creati numele grupei noi:  ");
            grupa = sc.next();
        } else if (choice.equals("da")) {
            System.out.print("Creati numele grupei noi:  ");
            grupa = sc.next();
        } else if (choice.equals("Nu")) {
            System.out.println("Iesire din program...");
            System.exit(0);
        }  else if (choice.equals("nu")) {
            System.out.println("Iesire din program...");
            System.exit(0);
        }

        System.out.println("Introducerea studentilor grupei " + grupa);
        System.out.print("Introduceti numele studentului 1: ");
        String name = sc.next();
        System.out.print("Introduceti anul nasterii: ");
        int year = sc.nextInt();
        System.out.println("Student: " + name + ", Varsta: " + (Calendar. getInstance(). get(Calendar. YEAR)-year));
    }
}
