import java.util.Scanner;

public class Exc2 {
    public static void main(String[] args) {
        int [] array = new int[5];

        Scanner obj1 = new Scanner(System.in);

        //get the array of numbers:
        for (int i=0; i <= array.length-1; i++){
            array[i] = obj1.nextInt();
        }

        //find the maximum value
        int max = 0;
        for (int i=0; i <= array.length-1; i++){

            if (array[i]>=max){
                max = array[i];
            }

        }
        System.out.println("max= " + max);
        //find the minimum value
        int min=array[1];
        for (int i=0; i <= array.length-1; i++){

            if (array[i]<=min){
                min = array[i];
            }

        }
        System.out.println("min= " + min);

        //find the middle number
        int sum=0;
        int middleNumber;
        for (int i=0; i <= array.length-1; i++){
            sum+=array[i];
        }
        middleNumber = sum/array.length;

        System.out.println("middle Number = " + middleNumber);
    }

}
