import java.util.Arrays;

public class FromOop {
    public static void main(String[] args) {
        Dog dog1 = new Dog(11, "DogName1");
        Dog dog2 = new Dog(4,"Sam");
        Dog dog3 = new Dog(13, "Bob");

        int [] arrayDogsAge = {dog1.age, dog2.age, dog3.age};
        String [] arrayDogsName = {dog1.name, dog2.name, dog3.name};

        //display dogs age < 10 years
        for (int i=0; i <= arrayDogsAge.length-1; i++){
            if (arrayDogsAge[i] >10){
                System.out.println(arrayDogsAge[i]);
            }
        }


        //display dogs sorted descending by age
        for (int i = 0; i<=arrayDogsAge.length-1; i++){
            for (int j = i+1; j<=arrayDogsAge.length-1;j++) {
                int tmp = 0;
                if (arrayDogsAge[i] < arrayDogsAge[j]) {
                    tmp = arrayDogsAge[i];
                    arrayDogsAge[i] = arrayDogsAge[j];
                    arrayDogsAge[j] = tmp;
                }
            }
        }
        System.out.println(Arrays.toString(arrayDogsAge));

        //display Dog that has name Sam
        if (dog1.getDogName()=="Sam"){
            System.out.println("Dog1 has name" + dog1.getDogName());
        }
        else if (dog2.getDogName()=="Sam"){
            System.out.println("Dog2 has name" + dog2.getDogName());
        }
        else if (dog3.getDogName()=="Sam"){
            System.out.println("Dog3 has name" + dog3.getDogName());
        }
        else System.out.println("no dog name Sam is present");
    }
}
