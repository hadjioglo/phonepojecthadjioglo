import java.util.Scanner;

public class Exc3 {
    //program that calculates the salary of a worker.
    // The program asks to input hours worked in a month and returns the amount
    // rate $10 for 1 hour

    public static void main(String[] args) {
        Scanner obj1 = new Scanner(System.in);
        System.out.println("Input. How many hours have you worked this month? ");
        int hours = obj1.nextInt();
        System.out.println("Your salary is: $" + hours * 10 + " (rate = $10/hour)");

    }
}
