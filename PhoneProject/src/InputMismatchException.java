import java.util.Scanner;

public class InputMismatchException {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        boolean continueInput = true;

        do{
            try{
                System.out.println("Enter an integer ");
                int number = input.nextInt();

                //display the result
                System.out.println("Entered number is " + number);
                continueInput = false;
            } catch (java.util.InputMismatchException ex){
                System.out.println("Try again + (" + "Your input is invalid. is necessary " + "the int number)");
                input.nextLine();
            }
        }while (continueInput);
    }
}
