import java.util.*;

public class User {
    public static void main(String[] args) {
        Scanner inputName = new Scanner(System.in);
        Scanner inputLastName = new Scanner(System.in);

        boolean continueInput = true;

        do{
            try{
                System.out.println("Enter your name ");
                String name = inputName.nextLine();
                System.out.println("Enter your Last name ");
                String lastName = inputLastName.nextLine();

                //display the result
                System.out.println("Entered name is " + name);
                System.out.println("Entered Last Name is " + lastName);
                continueInput = false;
            } catch (java.util.InputMismatchException ex){
                System.out.println("Try again + (" + "Your input is invalid. is necessary " + "only letters, but you entered characters)");
                inputName.nextLine();
                inputLastName.nextLine();
            }
        }while (continueInput);
    }
}
