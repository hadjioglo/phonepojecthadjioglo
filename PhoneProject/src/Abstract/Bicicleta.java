package Abstract;

abstract class Bicicleta {
    Bicicleta(){ //constructor
        System.out.println("Bicycle is created");
    }
    abstract void run(); //abstract method
    void schimbaViteza(){ //non-abstract method
        System.out.println("the speed is changed");
    }
}

class Honda extends Bicicleta{
    void run(){ //add body to abstarct method run()
        System.out.println("merge in siguranta..");
    }
}

class Abstraction{
    public static void main(String[] args) {
        Bicicleta obj = new Honda();
        obj.run();
        obj.schimbaViteza();
    }
}