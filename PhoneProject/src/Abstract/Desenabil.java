package Abstract;

//declare the interface from user 1
interface Desenabil {
    void deseneaza();
}

//implement by user 2
class Patrat implements Desenabil{
    public void deseneaza(){
        System.out.println("deseneaza un patrat");
    }
}

class Cerc implements Desenabil{
    public void deseneaza(){
        System.out.println("deseneaza un cerc");
    }
}

//utilizarea interfetei de catre a 3-lea user
class Interface{
    public static void main(String[] args) {
        Desenabil d = new Cerc();
        d.deseneaza();
    }
}
