public class Main {
    public static void main(String[] args) {
        Phone obj1 = new Phone(69556677, "Samsung S21", 200.6);
        Phone obj2 = new Phone(69556678, "Samsung S21");
        Phone obj3 = new Phone();
        Phone obj4 = new Phone();

        obj1.number = 69556677;
        obj2.number = 69556678;
        obj3.number = 69556679;

        System.out.println("Phone number = " + obj1.number);
        obj2.model = "Samsung S21";
        System.out.println("Model: " + obj2.model);
        obj3.weight = 120.6;
        System.out.println("Weight: " + obj3.weight);
        System.out.println();

        obj1.receiveCall("Igor");
        System.out.println("His phone number: " + obj1.getNumber(obj1.number));

        obj2.receiveCall("Alex");
        System.out.println("His phone number: " + obj2.getNumber(obj2.number));

        obj3.receiveCall("Mircea");
        System.out.println("His phone number: " + obj3.getNumber(obj3.number));

        obj1.receiveCall("Fiodor", 57948644);

        System.out.println(obj4.getNumber(54436));

        obj3.sendMessage(4354554,44343534,544353);

    }
}
