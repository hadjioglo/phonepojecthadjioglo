package Author;

public class Book {

    private String name;
    private String author;
    private double price;
    private int qtyInStock;

    public Book(String name, Author author, double price){
        this.name = name;
        this.author = new String();
        this.price = price;
    }
    public Book(String name, Author author, double price, int qtyInStock){
        this.name = name;
        this.author = String.valueOf(author);
        this.price = price;
        this.qtyInStock = qtyInStock;

    }

    public String getName(){
        return name;
    }

    public String getAuthor(){
        return author;
    }

    public double getPrice(){
        return price;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public double getQty(){
        return qtyInStock;
    }

    public void setQty(int qtyInStock){
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return "Book name: \"" + name + "\" written by: "+ author +" price: " + price + " available in Stock:" + qtyInStock;
    }

}
