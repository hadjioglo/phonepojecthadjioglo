package Author;

public class TestAuthor {
    public static void main(String[] args) {
        Author anAuthor = new Author("Alan Kay", "alankay@books.com", 'm');
        System.out.println(anAuthor); //call toString()
        anAuthor.setEmail("alankay88@gmail.com");
        System.out.println(anAuthor);

    }
}
