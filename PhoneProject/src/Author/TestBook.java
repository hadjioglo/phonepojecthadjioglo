package Author;

public class TestBook {
    public static void main(String[] args) {
        Author anAuthor = new Author("Alan Kay", "alankay@books.com", 'm');
        Book aBook = new Book("Java pentru incepatori", anAuthor, 19.95, 1000);
        Book anotherBook = new Book ("mai multe Java pentru incepatori", new Author(), 29.95, 850);

        System.out.println(aBook.getAuthor());
        System.out.println(anAuthor.getEmail());
        System.out.println(anotherBook.getAuthor());
    }
}
