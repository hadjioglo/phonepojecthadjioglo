import java.util.Scanner;

public class SwitchExample {
    public static void main(String[] args) {

        Scanner obj1 = new Scanner(System.in);
        System.out.println("Enter a number from 0 to 6");
        int n = obj1.nextInt();


        switch (n){
            case 0:
                System.out.println("Mo");
                break;
            case 1:
                System.out.println("Tu");
                break;
            case 2:
                System.out.println("We");
                break;
            case 3:
                System.out.println("Th");
                break;
            case 4:
                System.out.println("Fr");
                break;
            case 5:
                System.out.println("Sa");
                break;
            case 6:
                System.out.println("Su");
                break;
            default:
                System.out.println("Not a valid number");
        }
    }
}
