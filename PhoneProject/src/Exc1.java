import java.util.Scanner;

public class Exc1 {
    public static void main(String[] args) {
        System.out.println("Enter a number INT");
        Scanner obj1 = new Scanner(System.in);

        int number = obj1.nextInt();

        System.out.println("Original number: " + number);

        int reverseNumber=0;
        int sum = 0;

        while(number >0){
            int remainder = number %10;
            reverseNumber*=10;
            reverseNumber+=remainder;
            number/=10;
            sum+=remainder;

        }

        System.out.println("Reverse number : " + reverseNumber);
        System.out.println("Sum of digits : " + sum);
    }


}
