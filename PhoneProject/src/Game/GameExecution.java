package Game;

import java.util.Scanner;

public class GameExecution {
    public static void main(String[] args) {
        System.out.print("Introduce player 1: ");
        Scanner obj3 = new Scanner(System.in);

        //create object for player1
        OldVersion obj1 = new OldVersion();

        String player1 = obj1.setPlayer(obj3.nextLine());
        System.out.print(player1);
        obj1.addCrystal();

        System.out.print("Introduce player 2: ");
        Scanner obj4 = new Scanner(System.in);

        //create object for player2
        NewVersionGame obj2 = new NewVersionGame();

        String player2 = obj2.setPlayer(obj4.nextLine());
        System.out.print(player2);
        //call method crystals from newVersionGame
        obj2.addCrystal();

    }
}
